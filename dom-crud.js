// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const newA = document.createElement('a');
newA.innerText = 'Buy Now!';
//newA.id = 'cta';
newA.setAttribute("id", "cta");
const main = document.querySelector('main');
main.appendChild(newA);

//------------ const lastP = document.getElementsByTagName('main')

// Access (read) the data-color attribute of the <img>,
// log to the console
const getImg = document.getElementsByTagName('img')[0];
console.log(getImg.dataset.color);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const fEl = document.getElementById('features');
const li3 = fEl.getElementsByTagName('li')[2];
li3.className = 'highlight';  //set the className
//li3.classList.add('highlight'); //add the class 


// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const pElm = document.getElementsByTagName('p');
const lastP = pElm[pElm.length -1];
main.removeChild(lastP);
