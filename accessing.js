// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"

// const h2Elm = document.getElementsByTagName('h2')[0];
// const h2Elm = document.getElementById('weather-head');
const h2Elm = document.querySelector('h2');
h2Elm.innerText = 'February 10 Weather Forecast, Seattle';
h2Elm.style.color = 'green';


// Change the styling of every element with class "sun" to set the color to "orange"
const y = document.getElementsByClassName('sun');
Array.from(y).forEach(e => {
    e.style.color = 'orange';
})


// Change the class of the second <li> from to "sun" to "cloudy"
const liElm = document.getElementsByTagName('li')[1];
liElm.className = 'cloudy';
//liElm.classList.remove('sun');
//liElm.classList.add('cloudy');


//----------- add a new li
const ulElm = document.getElementById("weather");
const newLi = document.createElement('li');
//newLi.className = 'sun';
newLi.setAttribute("class", "sun");
newLi.appendChild(document.createTextNode('Thursday'));
ulElm.appendChild(newLi);

