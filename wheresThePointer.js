// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


// const tbody = document.querySelector('tbody');
// tbody.addEventListener( 'click', function(e){
//     console.log(e);
//     const td = e.target;  // e.target.tagName;
//     td.innerText = e.clientX + ', '+ e.clientY;

// })

const x = document.getElementsByTagName('td');
Array.from(x).forEach( y => {
    y.addEventListener ('click', (e) => {
        console.log(e);
        const z = e.target;  // e.target.tagName;
        z.innerText = e.clientX + ', '+ e.clientY;
    })
})
