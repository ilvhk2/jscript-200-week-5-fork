// If an li element is clicked, toggle the class "done" on the <li>
const l = document.querySelector('li');

l.addEventListener('click', toggleCls);
function toggleCls() {
     this.classList.toggle("done");
}

// If a delete link is clicked, delete the li element / remove from the DOM
const liA = document.querySelector('.delete');
liA.addEventListener('click', remove);

function remove(){
    this.parentNode.remove();
    console.log('** data removed ');
}

// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
const addLi = document.querySelector(".add-item");

const addListItem = function(e) {
    e.preventDefault();
    const input = this.parentElement.parentElement.getElementsByTagName('input')[0];
    let text = input.value; 
    // Finish function here  
     let ulElm = e.target.parentElement.previousElementSibling;
     if (text) {
        const  newLi = document.createElement("li"); 
          //<span>Reading</span>
        const span = document.createElement("span");
        span.textContent = text + ' ';
        newLi.appendChild(span);
          //-----  <a class="delete">Delete</a>
        const link = document.createElement('a');
        link.textContent="Delete";
          // create a delete class
        clsDelete = document.createAttribute("class");
        clsDelete.value = "delete";
         // add "delete" class to <a>
        link.setAttributeNode(clsDelete);
        link.addEventListener('click',remove);
        newLi.appendChild(link);
        newLi.addEventListener('click', toggleCls);
        ulElm.appendChild(newLi);
    } else {
        alert('enter text in the inbox before click ADD') ;
    }
  }
  addLi.addEventListener('click', addListItem)

  // SORT LI
const h = document.querySelector("h1");
const temp=h.innerHTML + "<button id='sort' type='button' style='height: 30px; background-color: lightgreen;'>Sort it!</button>"
h.innerHTML = temp;

const sort = document.querySelector("#sort");
sort.addEventListener('click',sortList )

function sortList() {
    let list, i, switching, b, shouldSwitch;
    list = document.querySelector(".today-list");
 
    switching = true;
    while (switching) {
        switching = false;
        b = list.children;

        for (i = 0; i < (b.length - 1); i++) {
          shouldSwitch = false;
          if (b[i].innerText.toLowerCase() > b[i + 1].innerText.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {
          b[i].parentNode.insertBefore(b[i + 1], b[i]);
          switching = true;
        }
    }
}
